# Bilibili Helper For Seewo

## 介绍
**注意: 对于非希沃或非触屏设备, 安装此扩展十分多余，没有任何用处！！**

在[希沃电脑](https://www.seewo.com)上全屏 [bilibili](https://www.bilibili.com) 的视频并不十分容易，原因在于点击视频时无菜单弹出。
该扩展用于解决此问题。
注：该扩展为 [chromium](https://www.chromium.org/) 系扩展，可安装在任意 chromium 系的浏览器上。

## 下载
[Github Release](https://github.com/Bluevect/BilibiliHelperForSeewo/releases)

[Gitee Release](https://gitee.com/bluevect/BilibiliHelperForSeewo/releases)

## 使用
1. 安装扩展
  - 注意: 该扩展无法在[希沃浏览器](https://e.seewo.com/#footer)(希沃原装浏览器)上安装，**推荐安装 _[Microsoft Edge](https://www.microsoft.com/en-us/edge)_ 而不使用希沃浏览器。** 原因在于希沃浏览器使用了旧版的 chromium, 扩展图标不显示，十分沙雕。另一方面，在 Microsoft Edge 上键盘会自动弹出，希沃浏览器不会（这点十分影响体验）。
2. 打开 Bilibili 视频
3. 点击扩展图标进行全屏 （或右键使用菜单全屏）

## Credits
- [Chrome Extension Development Documentation](https://developer.chrome.com/docs/extensions/)
- 图标来源 https://www.bilibili.com/favicon.ico?v=1